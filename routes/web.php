<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\UserController;

Route::get('/blog/create', [BlogController::class, 'create'])->middleware('auth');
Route::get('//blog/update/{id}', [BlogController::class, 'update'])->middleware('auth');
Route::get('/blog', [BlogController::class, 'view'])->middleware('auth');
Route::post('/blog/add', [BlogController::class, 'store'])->middleware('auth');
Route::get('/', [BlogController::class, 'index']);
Route::put('/blog/{id}', [BlogController::class, 'edit'])->middleware('auth');
Route::delete('/blog/{id}', [BlogController::class, 'destroy'])->middleware('auth');


Route::get('/registration', [UserController::class, 'registration']);
Route::get('/login', [UserController::class, 'login']);
Route::post('/user_registration', [UserController::class, 'registerUser']);
Route::post('/authentication', [UserController::class, 'authentication']);
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');
