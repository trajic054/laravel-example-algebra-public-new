@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="mb-3">
            <h2>Ažuriraj članak</h2>
        </div>
        <form method="POST" action="/blog/{{$article->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naziv članka</label>
              <input type="text" name="naslov" value="{{$article->naslov}}" class="form-control" id="exampleInputName" aria-describedby="nameNaslov">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Tekst članka</label>
                <input type="text" name="tekst" value="{{$article->tekst}}" class="form-control" id="exampleInputYear" aria-describedby="nameTekst">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection