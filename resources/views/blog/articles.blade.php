@extends('layouts.default')

@section('content')

    @auth
    <div class="row">
        <div class="col-sm-2">
            <a class="btn btn-primary" href="/blog/create">Dodaj novi članak</a>
        </div>
    </div>
    @endauth
    <div class="row">
        <div class="col-sm-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Naziv članka</th>
                    <th scope="col">Tekst članka</th>
                    @auth
                        <th scope="col">Akcije</th>
                    @endauth
                </tr>
                </thead>
                <tbody>
                    @foreach ($articles as $key=>$article)
                        <tr>
                            <th scope="row">{{($key+1)}}</th>
                            <td>{{$article->naslov}}</td>
                            <td>{{$article->tekst}}</td>
                            @auth
                            <td>
                                <a class="btn btn-secondary" href="/blog/update/{{$article->id}}">Uredi članak</a>
                                <form method="POST" action="/blog/{{$article->id}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Izbrisi članak</a>
                                </form>
                            </td>
                            @endauth
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection