@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="mb-3">
            <h2>Unesi novi članak</h2>
        </div>
        <form method="POST" action="/blog/add">
            @csrf
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naziv članka</label>
              <input type="text" name="naslov" class="form-control" id="exampleInputName" aria-describedby="nameNaslov">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Tekst članka</label>
                <input type="text" name="tekst" class="form-control" id="exampleInputYear" aria-describedby="nameTekst">
            </div>
            <button type="submit" class="btn btn-primary">Predaj</button>
        </form>
    </div>
@endsection