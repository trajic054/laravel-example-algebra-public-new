@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="mb-3">
            <h2>Login</h2>
        </div>
        <form method="POST" action="/authentication">
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email</label>
                <input type="email" name="email" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Lozinka</label>
                <input type="password" name="password" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection