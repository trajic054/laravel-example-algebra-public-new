<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function view()
    {
        return view('blog.articles',
        [
            'articles' => Blog::all()
        ]);
    }

    public function index()
    {
        return view('blog.articles',
        [
            'articles' => Blog::all()
        ]);
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(Request $request)
    {
        $formFields = $request->validate([
            'naslov' => 'required',
            'tekst' => 'required'
        ]);

        Blog::create($formFields);

        return redirect('/');
    }

    public function update($id)
    {
        $article = Blog::find($id);
        return view('blog.update',
        [
            'article' => $article
        ]);
    }

    public function edit(Request $request, $id)
    {
        $article = Blog::find($id);

        $article['naslov'] = $request->naslov;
        $article['tekst'] = $request->tekst;

        $article->save();
        return redirect('/');
    }

    public function destroy($id)
    {
        $article = Blog::find($id);
        $article->delete();
        return redirect('/');
    }
}
